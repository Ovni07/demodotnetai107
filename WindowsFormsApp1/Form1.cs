﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            txtNom.TextChanged += TxtNom_TextChanged;
        }

        private void TxtNom_TextChanged(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int r = rnd.Next(0, 256);
            int v = rnd.Next(0, 256);
            int b = rnd.Next(0, 256);

            this.BackColor = Color.FromArgb(r, v, b);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Bonjour " + txtNom.Text);
        }
    }
}
