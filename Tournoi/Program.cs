﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.AFCEPF.IntroCSharp.MesOutils;

namespace Fr.AFCEPF.IntroCSharp.Tournoi
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1 - ajouter une référence à l'assembly à utiliser
            // 2 - vérifier que la classe à utiliser est bien public
            // 3 - ajouter une directive using sur le namespace à utiliser

            Calendrier c = new Calendrier();

            int annee = 2020;

            string message = string.Format("L'année {0} {1} bissextile", 
                               annee, 
                               (c.EstBissextile(annee)) ? "est" : "n'est pas"
                             );

            Console.WriteLine(message);

            Console.ReadLine();
        }
    }
}
